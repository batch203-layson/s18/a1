// console.log("Hello World");



function displaySum(addendOne, addendTwo){
    let sumOfTwoNums = addendOne + addendTwo;
    console.log("Displayed sum of " +addendOne+ " and " +addendTwo);
    return sumOfTwoNums;
}

console.log(displaySum(5, 15));


function displayDifference(minuendNum, subtrahendNum){
    let differenceOfTwoNums = minuendNum - subtrahendNum;
    console.log("Displayed difference of " +minuendNum+ " and " +subtrahendNum);
    return differenceOfTwoNums;
}

console.log(displayDifference(20, 5));



function displayProduct(multiplicandNum, multiplierNum){
    let productOfTwoNums = multiplicandNum * multiplierNum;
    console.log("The product of " +multiplicandNum+ " and " +multiplierNum);
    return productOfTwoNums;
}

console.log(displayProduct(50, 10));


function displayQuotient(dividendNum, divisorNum){
    let quotientOfTwoNums = dividendNum / divisorNum;
    console.log("The quotient of " +dividendNum+ " and " +divisorNum);
    return quotientOfTwoNums;
    
    
}
// console.log("The quotient of 50 and 10");
console.log(displayQuotient(50, 10));




function displayCircleArea(radiusNum){
    let areaOfCircle = radiusNum * radiusNum * 3.14159;
    console.log("The result of getting the area of a circle with " +radiusNum+ " raduis:");
    return areaOfCircle;
}

console.log(displayCircleArea(15));



function displayAverageOfFourNums(numOne, numTwo, numThree, numFour){
    let averageOfFourNums = (numOne + numTwo + numThree + numFour) /4;
    console.log("The average of " +numOne+ "," +numTwo+ "," +numThree+ " and " +numFour+":");
    return averageOfFourNums;
}
console.log(displayAverageOfFourNums(20, 40, 60, 80));



function displayScoreIfPass(scoreNum, totScore) {
    let computeResult = (scoreNum / 50) * 100;
    let passScore = computeResult > totScore;
    console.log("Is "+scoreNum+ "/" +totScore+ " a passing score?");
    return passScore;
  }
  
  console.log(displayScoreIfPass(38, 75));